package rtree.estructura;

/**
 * Created by elbraulio on 28-10-16.
 */
public class Rectangulo {
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private int x3;
    private int y3;
    private int x4;
    private int y4;

    /**
     * (x4,y4)              (x3,y3)
     * ----------------------
     * |                    |
     * |                    |
     * ----------------------
     * (x1,y1)               (x2,y2)
     */
    public Rectangulo(int x1, int y1, int x3, int y3) {
        setRectangulo(x1, y1, x3, y3);
    }

    public Rectangulo getMBR(Rectangulo rectangulo) {

        int y1m = Math.min(y1, rectangulo.getY1());
        int x1m = Math.min(x1, rectangulo.getX1());
        int y3m = Math.max(y3, rectangulo.getY3());
        int x3m = Math.max(x3, rectangulo.getX3());

        return new Rectangulo(x1m, y1m, x3m, y3m);
    }

    public int getArea() {
        return (x2 - x1)*(y3 - y2);
    }

    public void setRectangulo(int x1, int y1, int x3, int y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x3 = x3;
        this.y3 = y3;

        //Obtenemos coordenadas que faltan
        x2 = x3;
        y2 = y1;
        x4 = x1;
        y4 = y3;
    }

    public void setRectangulo(Rectangulo rectangulo) {
        setRectangulo(rectangulo.x1, rectangulo.y1, rectangulo.x3, rectangulo.y3);
    }

    public boolean intersect(Rectangulo rectangulo) {
        int rWith = rectangulo.x3 - rectangulo.x1;
        int rHeight = rectangulo.y4 - rectangulo.y1;
        int with = x3 - x1;
        int height = y4 - y1;
        return x1 < rectangulo.x1 + rWith && x1 + with > rectangulo.x1 && y1 < rectangulo.y1 + rHeight && y1 + height > rectangulo.y1;

    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX3() {
        return x3;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public int getY3() {
        return y3;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    public int getX4() {
        return x4;
    }

    public void setX4(int x4) {
        this.x4 = x4;
    }

    public int getY4() {
        return y4;
    }

    public void setY4(int y4) {
        this.y4 = y4;
    }


}
