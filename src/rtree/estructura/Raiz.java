package rtree.estructura;

import rtree.algoritmo.MOverFlow;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by elbraulio on 28-10-16.
 */
public class Raiz extends RTree{

    private ArrayList<RTree> hijos = new ArrayList<>();

    /**
     * (x4,y4)              (x3,y3)
     * ----------------------
     * |                    |
     * |                    |
     * ----------------------
     * (x1,y1)               (x2,y2)
     *
     * @param x1
     * @param y1
     * @param x3
     * @param y3
     */
    public Raiz(int x1, int y1, int x3, int y3) {
        super(x1, y1, x3, y3);

    }

    @Override
    public ArrayList<RTree> getHijos() {
        return hijos;
    }

    @Override
    public void recalcularMbr() {
        setRectangulo(hijos.get(0));
        for (RTree t : hijos) {
            setRectangulo(getMBR(t));
        }
    }

    @Override
    public void setHijos(ArrayList<RTree> hijos) {
        hijos.clear();
        hijos.addAll(hijos);
    }

    @Override
    public void insert(Rectangulo rectangulo) throws MOverFlow {

        try {
            if (hijos.size() == 0) {
                hijos.add(new Hoja(rectangulo));
                setRectangulo(rectangulo);
            } else if (hijos.get(0).isHoja()) {
                hijos.add(new Hoja(rectangulo));
                Rectangulo newMBR = getMBR(rectangulo);
                setRectangulo(newMBR);
            } else {
                ArrayList<RTree> hijosIguales = new ArrayList<>();
                RTree hijoMbrMin = hijos.get(0);
                int mbrArea;
                int crecimiento;
                int minCrecimiento = hijoMbrMin.getMBR(rectangulo).getArea() - hijoMbrMin.getArea();

                for (RTree t : hijos) {
                    mbrArea = t.getMBR(rectangulo).getArea();
                    crecimiento = mbrArea - t.getArea();

                    if (crecimiento < minCrecimiento) {
                        minCrecimiento = crecimiento;
                        hijosIguales.clear();
                        hijosIguales.add(t);
                    } else if (crecimiento == minCrecimiento) {
                        hijosIguales.add(t);
                    }
                }

                int index = new Random().nextInt(hijosIguales.size());
                hijosIguales.get(index).insert(rectangulo);

                Rectangulo newMBR = getMBR(rectangulo);
                setRectangulo(newMBR);
            }
        } catch (MOverFlow over) {

        }

       //check overflow

    }


    @Override
    public ArrayList<Hoja> buscar(Rectangulo rectangulo) {

        ArrayList<Hoja> result = new ArrayList<>();

        if(intersect(rectangulo)) {

            for (RTree t : hijos) {
                result.addAll(t.buscar(rectangulo));
            }
        }

        return result;
    }
}
