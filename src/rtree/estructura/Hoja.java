package rtree.estructura;

import rtree.algoritmo.MOverFlow;

import java.util.ArrayList;

/**
 * Created by elbraulio on 28-10-16.
 */
public class Hoja extends RTree{

    /**
     * (x4,y4)              (x3,y3)
     * ----------------------
     * |                    |
     * |                    |
     * ----------------------
     * (x1,y1)               (x2,y2)
     *
     * @param x1
     * @param y1
     * @param x3
     * @param y3
     */
    public Hoja(int x1, int y1, int x3, int y3) {
        super(x1, y1, x3, y3);
    }

    public Hoja(Rectangulo rectangulo) {
        super(rectangulo);
    }

    @Override
    public ArrayList<RTree> getHijos() {
        return null;
    }

    @Override
    public void recalcularMbr() {

    }

    @Override
    public void setHijos(ArrayList<RTree> hijos) {

    }

    @Override
    public boolean isHoja() {
        return true;
    }

    @Override
    public void insert(Rectangulo rectangulo) throws MOverFlow {

    }

    @Override
    public ArrayList<Hoja> buscar(Rectangulo rectangulo) {

        ArrayList<Hoja> result = new ArrayList<>();

        if(intersect(rectangulo)) {
            result.add(this);
        }

        return result;
    }

}
