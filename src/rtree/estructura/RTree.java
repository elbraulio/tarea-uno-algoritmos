package rtree.estructura;

import rtree.algoritmo.Algoritmo;
import rtree.algoritmo.LinearSplit;
import rtree.algoritmo.MOverFlow;

import java.util.ArrayList;

/**
 * Created by elbraulio on 28-10-16.
 */
public abstract class RTree extends Rectangulo{

    protected final int M = 3;
    private Algoritmo algoritmo = new LinearSplit();

    /**
     * (x4,y4)              (x3,y3)
     * ----------------------
     * |                    |
     * |                    |
     * ----------------------
     * (x1,y1)               (x2,y2)
     *
     * @param x1
     * @param y1
     * @param x3
     * @param y3
     */
    public RTree(int x1, int y1, int x3, int y3) {
        super(x1, y1, x3, y3);
    }

    public RTree(Rectangulo rectangulo) {
        super(rectangulo.getX1(), rectangulo.getY1(), rectangulo.getX3(), rectangulo.getY3());
    }

    public abstract ArrayList<RTree> getHijos();

    public abstract void recalcularMbr();

    public abstract void setHijos(ArrayList<RTree> hijos);

    public boolean isHoja(){
        return false;
    }

    public abstract void insert(Rectangulo rectangulo) throws MOverFlow;

    public abstract ArrayList<Hoja> buscar(Rectangulo rectangulo);

    public void setAlgoritmo(Algoritmo newAlg) {
        algoritmo = newAlg;
    }

    public Algoritmo getAlgoritmo() {
        return algoritmo;
    }

}
