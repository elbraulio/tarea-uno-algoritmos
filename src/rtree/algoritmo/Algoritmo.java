package rtree.algoritmo;

import rtree.estructura.RTree;

import java.util.ArrayList;

/**
 * Created by elbraulio on 03-11-16.
 */
public interface Algoritmo {
    void setArray(ArrayList<RTree> hijos);

    ArrayList<RTree> getArrayHijo1();

    ArrayList<RTree> getArrayHijo2();
}
