package rtree.algoritmo;

import rtree.estructura.RTree;
import rtree.estructura.Rectangulo;

import java.util.ArrayList;

/**
 * Created by elbraulio on 03-11-16.
 */
public class LinearSplit implements Algoritmo {

    private int m = 0;
    private int index1, index2;

    private ArrayList<RTree> hijo1 = new ArrayList<>(), hijo2 = new ArrayList<>();

    private ArrayList<RTree> hijos;
    //esto es para la distancia maxima
    private int maxX1, minX2, minY4, maxY1;

    //esto es para el rango
    private int minX1, maxX2, maxY4, minY1;

    //rectangulos
    private RTree rectMaxX1, rectMinX2, rectMinY4, rectMaxY1;

    private RTree rect1, rect2;

    @Override
    public void setArray(ArrayList<RTree> hijos) {
        this.hijos = hijos;

        RTree primero = hijos.get(0);

        maxX1 = primero.getX1();
        minX2 = primero.getX2();
        minY4 = primero.getY4();
        maxY1 = primero.getY1();

        minX1 = maxX1;
        maxX2 = minX2;
        maxY4 = minY4;
        minY1 = maxY1;
        int i1 = 0, i2 = 0, i3 = 0, i4 = 0;
        int i = 0;

        for (RTree t : hijos) {
            /* DISTANCIA */
            if (t.getX1() > maxX1) {
                maxX1 = t.getX1();
                rectMaxX1 = t;
                i1 = i;
            }
            if (t.getX2() < minX2) {
                minX2 = t.getX2();
                rectMinX2 = t;
                i2 = i;
            }
            if (t.getY4() < minY4) {
                minY4 = t.getY4();
                rectMinY4 = t;
                i3 = i;
            }
            if (t.getY1() > maxY1) {
                maxY1 = t.getY1();
                rectMaxY1 = t;
                i4 = i;
            }

            /* RANGO */
            if (t.getX1() < minX1) {
                minX1 = t.getX1();

            }
            if (t.getX2() > maxX2) {
                maxX2 = t.getX2();

            }
            if (t.getY4() > maxY4) {
                maxY4 = t.getY4();

            }
            if (t.getY1() < minY1) {
                minY1 = t.getY1();

            }
            i++;
        }

        //elegimos los rectangulos
        double dx = (maxX1 - minX2) / (maxX2 - minX1);
        double dy = (maxY1 - minY4) / (maxY4 - minY1);

        if (dx >= dy) {
            rect1 = rectMinX2;
            rect2 = rectMaxX1;
            index1 = i2;
            index2 = i1;
        } else {
            rect1 = rectMaxY1;
            rect2 = rectMinY4;
            index1 = i4;
            index2 = i3;
        }

        hijo1.add(rect1);
        hijo2.add(rect2);
        Rectangulo mbr1 = new Rectangulo(rect1.getX1(), rect1.getY1(), rect1.getX3(), rect1.getY3());
        Rectangulo mbr2 = new Rectangulo(rect2.getX1(), rect2.getY1(), rect2.getX3(), rect2.getY3());

        for (int j = 0; j < hijos.size(); j++) {
            if (j != index1 && j != index2) {
                RTree current = hijos.get(j);
                int dif1 = mbr1.getMBR(current).getArea() - mbr1.getArea();
                int dif2 = mbr2.getMBR(current).getArea() - mbr2.getArea();

                if (dif1 > dif2) {
                    if(hijos.size() - hijo2.size() != m - hijo1.size()) {
                        hijo2.add(current);
                        mbr2 = mbr2.getMBR(current);
                    } else {
                        hijo1.add(current);
                        mbr1 = mbr1.getMBR(current);
                    }

                } else {
                    if(hijos.size() - hijo1.size() != m - hijo2.size()) {
                        hijo1.add(current);
                        mbr1 = mbr1.getMBR(current);
                    } else {
                        hijo2.add(current);
                        mbr2 = mbr2.getMBR(current);
                    }

                }
            }
        }


    }

    @Override
    public ArrayList<RTree> getArrayHijo1() {

        return hijo1;
    }

    @Override
    public ArrayList<RTree> getArrayHijo2() {

        return hijo2;
    }
}
