package rtree.algoritmo;

import rtree.estructura.RTree;

import java.util.ArrayList;

/**
 * Created by elbraulio on 03-11-16.
 */
public class MOverFlow extends Exception {
    private RTree overflow;

    public MOverFlow(RTree overflow) {
        this.overflow = overflow;
    }

    public RTree getRTree() {
        return overflow;
    }
}
